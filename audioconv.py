import tkinter as tk
from tkinter import filedialog
from pydub import AudioSegment

def convert_to_wav():
    input_file = filedialog.askopenfilename(title='Select M4A File')
    output_file = filedialog.asksaveasfilename(title='Save WAV File', defaultextension='.wav')

    audio = AudioSegment.from_file(input_file, format='m4a')
    audio.export(output_file, format='wav')

    status_label.config(text='Conversion completed!')

def exit_program():
    window.destroy()

# Create Tkinter window
window = tk.Tk()
window.title('M4A to WAV Converter')

# Set window size
window.geometry('480x480')

# Convert Button
convert_button = tk.Button(window, text='Convert to WAV', command=convert_to_wav)
convert_button.pack()

# Exit Button
exit_button = tk.Button(window, text='Exit', command=exit_program)
exit_button.pack()

# Status Label
status_label = tk.Label(window, text='')
status_label.pack()

# Run Tkinter event loop
window.mainloop()
